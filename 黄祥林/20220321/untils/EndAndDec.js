const crypto = require("crypto");
let EncAndDec = {

    md5: function (param) {

        let md5 = crypto.createHash("md5");
        md5.update(param);
        return md5.digest("hex");
    }


}

module.exports = EncAndDec;