let fs = require('fs');
let http = require('http');
let nunjucks =require('nunjucks');

 http.createServer(async function(req,res){
    let postData = '';
    req.on('data', function (chunk) {//异步模块
        postData += chunk;
    })
    req.on('end',async function(){

   
     let url =req.url;
     if(url.lastIndexOf('.')>-1){
         let path = '.'+url;
         fs.exists(path,function(result){
             if(!result){
                 path = './imgs/404.jpg';
             }else{
                 fs.readFile(path,function(err,data){
                     if(err){
                         res.end(err.message)
                     }else{
                         res.write(data);
                         res.end();
                     }
                 })
             }
         })
     }else{
         res.setHeader('Content-type','text/html;charset=utf8');
         let url = req.url;
         let query = url.split('?')[1];
         let queryArr = query.split('&');
         let needArr = [];
         if (req.method == "POST" && postData) {
            let postString = postData.split('&');//得到数据 [c=index,a=index];
            let postArr = [];//把query参数构建成key val格式 如 c=index&a=index 构建成 [ c: 'index', a: 'index' ]
            for (let postKey in postString) {
                let temArr2 = postString[postKey].split('=');
                postArr[temArr2[0]] = temArr2[1];
            }
            req.post = postArr;
        }
         for(let val in queryArr){
             let tempArr = queryArr[val].split('=');
             needArr[tempArr[0]]=tempArr[1];
         }
         req.query=needArr;
         let controName = needArr['c'];
         controName = controName.replace(controName[0],controName[0].toUpperCase()) + 'Controller';
         let path = './controller/'+controName;
         let controller = require(path);
         
         let action = needArr['a'];
         let obj = new controller(req,res,nunjucks);
         let result =await obj[action]();
         res.write(result);
        //  res.setHeader('cookie',['demo=123456','password=654321']);
         res.end();
     }
    })
 }).listen(8080);
 async function handle(obj,action){
     let result =await obj[action]();
     return result;
 }