class BaseController{
    constructor(req,res,nunjucks){
        this.req =req;
        this.res =res;
        this.nunjucks=nunjucks;
        this.nunjucks.configure('./html',{autoescape:true});
    }
    display(path,data){
        return this.nunjucks.render(path,data);
    }
}
module.exports=BaseController;