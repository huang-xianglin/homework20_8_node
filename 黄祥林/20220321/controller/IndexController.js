const BaseController = require("./BaseController");
const Base = require("../model/Base");

class IndexController extends BaseController{
    async index(){
        let model =new Base();
        let sql = 'select * from number where adds>?';
        try{
            // let date =this.req.query['date']?this.query['date']:'2022-03-16';
            let adds = '0';
            let data =await model.query(sql,adds);
            return this.display('deta.html',{key:data,title:'国内各地区疫情统计汇总'});
        }catch(err){
            console.log(err);
        }
    }
}
module.exports=IndexController;