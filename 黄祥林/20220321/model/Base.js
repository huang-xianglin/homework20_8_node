let mysql =require('mysql');
let db = require('../config/db');
class Base{
    async query(sql,value){
        let promise =new Promise(function(resolve,reject){
            let connection = mysql.createConnection({host:db.host,user:db.user,password:db.password,database:db.database})
            connection.connect();
            connection.query(sql,value,function(err,result){
                if(err){
                    reject(err.message)
                }else{
                    resolve(result);
                }
            })
            connection.end()
        })
        return promise;
    }
    select(){
        let sql = 'select * from '+this.table;
        return this.query(sql,'')
    }
}
module.exports=Base;