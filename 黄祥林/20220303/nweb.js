let http = require("http");
let server = http.createServer((req,res) => {
    console.log(req.url);
    res.setHeader("Content-Type", "text/html; charset=utf-8");
  
    if(req.url =='/demo1'){
        res.end("这是第一个文件")
    }else if(req.url =='/demo2'){
        res.end("这是第二个文件")
    }else if(req.url =='/demo3'){
        res.end("这是第三个文件")
    }
    else{
        res.end("这是目录请添加后缀demo{i}")
    }
})
server.on('connection',function(){
    console.log('我连接了');
})

server.listen(8080)