const BaseController = require("./BaseController");
const Base = require("../model/Base");

class IndexController extends BaseController{
    async index(){
        let model =new Base();
        let sql = 'select * from number where adds=?';
        try{
            // let adds =this.req.query['adds']?this.query['adds']:'1860';
            let adds = '1860';
            let data =await model.query(sql,adds);
            return this.display('deta.html',{key:data,title:'国内各地区疫情统计汇总'});
        }catch(err){
            console.log(err);
        }
    }
}
module.exports=IndexController;