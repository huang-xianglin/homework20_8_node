let fs = require('fs');
let http = require("http");

let server = http.createServer();

server.listen(8080);

server.on('request',function(req,res){
    let url = req.url;
    if(url.indexOf('.') >-1){
        // console.log(url);
        let path ='.' + url;
        readFileByPromise(path,res);
    }
});

let promise = function(fileName){
    return new Promise(function(resolve,reject){
        if(fileName != './favicon.ico'){
            fs.exists(fileName,function(result){
                if(!result){
                    fileName = './home/imgs/404.jpg';
                }
                fs.readFile(fileName,function(err,data){
                   resolve(data);
                })
            })
        }
        
    })
}
async function readFileByPromise(fileName,res){
    let data = await promise(fileName);
    res.write(data);
    res.end();
}