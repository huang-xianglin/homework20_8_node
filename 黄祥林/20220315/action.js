let fs = require('fs');
let http = require('http');

http.createServer(function(req,res){
    let url = req.url;

    if(url.lastIndexOf('.')>-1){
        let path = '.' + url;
        fs.exists(path,function(result){
            if(!result){
                path='./imgs/404.jpg';
            }
            fs.readFile(path,function(err,data){
                if(err){
                    res.send(err.massage)
                }else{
                    res.write(data);
                    res.end();
                }
                
            })
        })
    }else{
        let url = req.url;
        let query = url.split('?')[1];
        let queryArr = query.split('&');
        let needArr = [];
        // console.log(queryArr);
        for(let val in queryArr){
            let tempArr = queryArr[val].split('=');
            needArr[tempArr[0]]=tempArr[1];
            
        }
        console.log(needArr);
        req.query = needArr;
        let controName = needArr['c'];
        let path = './controller/'+ controName;
        console.log(controName);
        let controller = require(path);
        let action = needArr['a'];
        
        controller[action](req);
        // controller.index(req);
        res.end();
    }   
}).listen(8080);
