const mysql = require("mysql");

//配置数据库

const db=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'123456',
    database:'study'
})

//封装数据库操作
function query(sql,callback){
    db.connect();
    //[user,user.id]

    db.query(sql,params, function(err,results,fields){
        if(err){
            console.log(err.message);
            return;
        }
        callback && callback(results, fields);
        //results作为数据操作后的结果，fields作为数据库连接的一些字段

    })
    //释放资源
    db.end(function(err){
        if(err){
            console.log('关闭失败');
        }
       
    });
}

//查询

db.query('select * from user_table', [],function(result,fields){
    console.log('查询结果：');
    console.log(result);
});

// let sql="SELECT * FROM user_table where id=?";
// let SqlParams ={
//     id:5
// };
// db.query(sql,SqlParams,function(result,fields){
//     console.log('查询结果：');
//     console.log(result);
// })

//添加实例
//INSERT INTO user_table SET ? 快捷方式
//INSERT INTO user_table(username,password) VALUES(?,?)

let  addSql = 'INSERT INTO user_table SET ?';
let  addSqlParams ={
    username:'小海豚',
    password:'66666'
};
db.query(addSql,addSqlParams,function(result,fields){
    console.log('添加成功')
})

//更新实例

let  upSql = 'update user_table set username=?,password=? where id=?';
let  upSqlParams =['鲨鱼', '555555',3];
db.query(upSql,upSqlParams,function(result,fields){
    console.log('更新成功')
})

//删除实例
let  delSql = 'delete from user_table where id=?';

db.query(delSql,4,function(result,fields){
    console.log('删除成功')
})